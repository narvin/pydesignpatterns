Design Patterns
===============

Creational
----------
Instantiate classes or create objects.

* ☑ Factory Method
* ☑ Abstract Factory
* ☑ Builder
* ☑ Singleton
* ☐ Object Pool
* ☐ Prototype

Structural
----------
Organize different classes and objects to form larger structures and provide new
functionality.

* ☑ Adapter
* ☑ Bridge
* ☑ Composite
* ☑ Decorator
* ☐ Facade
* ☐ Flyweight
* ☐ Private Class Data
* ☑ Proxy

Behavioral
----------
Identify common communication patterns between objects.

* ☐ Chain of Responsibility
* ☑ Command
* ☐ Interpreter
* ☑ Iterator 
* ☐ Mediator
* ☐ Memento
* ☑ Null Object
* ☐ Observer
* ☑ State
* ☑ Strategy
* ☑ Template Method
* ☐ Visitor
