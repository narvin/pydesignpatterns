"""Adapter design pattern."""

from abc import abstractmethod
import sys
from typing import Optional

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol


class IWebResource(Protocol):
    """Interface of a web resource class."""

    @abstractmethod
    def connect(self) -> None:
        """Connect to the web resource."""

    @abstractmethod
    def download(self) -> str:
        """Download to the web resource."""


class WebResource(IWebResource):
    """Concrete web resource class."""

    def __init__(self, url: str) -> None:
        self.url = url
        self.is_connected = False

    def connect(self) -> None:
        print(f"Connecting to {self.url}...")
        self.is_connected = True

    def download(self) -> str:
        print(f"Downloading from {self.url}...")
        return f"{self.url.split('/')[-1]} is cool"


class IWebResourceRemoteProxy(Protocol):
    """Interface of a web resource remote proxy class."""

    remote_resource: IWebResource

    @property
    @abstractmethod
    def value(self) -> str:
        """The downloaded the resource and return it as a string."""


class WebResourceRemoteProxy(IWebResourceRemoteProxy):
    """Concrete web resource remote proxy class."""

    def __init__(self, resource_name: str) -> None:
        self.remote_resource = WebResource(f"https://example.com/{resource_name}")
        self._value: Optional[str] = None

    @property
    def value(self) -> str:
        if self._value is None:
            self._value = self._download()
        return self._value

    def _connect(self) -> None:
        """Connect to the web resource."""
        if not self.remote_resource.is_connected:
            self.remote_resource.connect()

    def _download(self) -> str:
        """Download the web resource."""
        self._connect()
        return self.remote_resource.download()


class IDatabaseQuery(Protocol):
    """Interface of a database query class."""

    query_src: str
    result: str
    num_records: int


class DatabaseQuery(IDatabaseQuery):
    """Concrete database query class."""

    def __init__(self, query_src: str) -> None:
        self.query_src = query_src
        print(f"Executing query '{self.query_src}'...")
        self.result = f"Results of executing '{self.query_src}'"
        self.num_records = 1000


class IDatabaseQueryVirtualProxy(Protocol):
    """Interface of a database query virtual proxy class."""

    @property
    @abstractmethod
    def query_obj(self) -> IDatabaseQuery:
        """Instantiate the query object if necessary, and return it."""


class DatabaseQueryVirtualProxy(IDatabaseQuery, IDatabaseQueryVirtualProxy):
    """Concrete database query virtual proxy class."""

    def __init__(self, query_src: str) -> None:
        self._query_obj: Optional[IDatabaseQuery] = None
        self._query_src = query_src

    @property
    def query_obj(self) -> IDatabaseQuery:
        if not self._query_obj:
            self._query_obj = DatabaseQuery(self._query_src)
        return self._query_obj

    @property
    def query_src(self) -> str:  # type: ignore
        """Call through to the database query object's `query_src` attribute."""
        return self.query_obj.query_src

    @property
    def result(self) -> str:  # type: ignore
        """Call through to the database query object's `result` attribute."""
        return self.query_obj.result

    @property
    def num_records(self) -> int:  # type: ignore
        """Call through to the database query object's `num_records` attribute."""
        return self.query_obj.num_records


class IFile(Protocol):
    """Interface of a file class."""

    file_path: str

    @abstractmethod
    def read(self) -> str:
        """Read in and return the contents of a file."""


class File(IFile):
    """Concrete file class."""

    def __init__(self, file_path: str) -> None:
        self.file_path = file_path

    def read(self) -> str:
        print(f"Reading from file '{self.file_path}'...")
        return "foobar"


class IFileProtectionProxy(Protocol):
    """Interface of a file protection proxy class."""

    file_obj: IFile


class FileProtectionProxy(IFile, IFileProtectionProxy):
    """Concrete file protection proxy class."""

    AUTHORIZED_USERS = ["good_user"]

    def __init__(self, user: str, file_path: str) -> None:
        self.user = user
        self.file_obj = File(file_path)

    @property
    def file_path(self) -> str:  # type: ignore
        """Call through to the file object's `file_path` attribute."""
        return self.file_obj.file_path

    def read(self) -> str:
        if self.user in self.AUTHORIZED_USERS:
            return self.file_obj.read()
        return ""
