"""Tests for the Singleton design pattern."""

import pytest

from design_patterns.singleton import DatabaseSingleton, ExpensiveSingleton


class TestDatabaseSingleton:
    """Test suite for the DatabaseSingleton class."""

    @staticmethod
    def test_get_instance() -> None:
        """Test that a DatabaseSingleton is only instantiated once."""
        inst1 = DatabaseSingleton.get_instance("my connection string")
        inst2 = DatabaseSingleton.get_instance("my other connection string")
        assert inst1 is inst2
        assert inst1.conn_str == "my connection string"
        assert inst2.conn_str == "my connection string"

    @staticmethod
    def test_fail_instantiate() -> None:
        """Test that a DatabaseSingleton can't be instantiated directly."""
        with pytest.raises(TypeError) as exc_info:
            DatabaseSingleton("my connection string")
        assert "can't be instantiated directly." in str(exc_info.value)


class TestExpensiveSingleton:
    """Test suite for the ExpensiveSingleton."""

    @staticmethod
    def test_get_instance() -> None:
        """Test that a ExpensiveSingleton is only instantiated once."""
        inst1 = ExpensiveSingleton.get_instance()
        inst2 = ExpensiveSingleton.get_instance()
        assert inst1 is inst2

    @staticmethod
    def test_fail_instantiate() -> None:
        """Test that a ExpensiveSingleton can't be instantiated directly."""
        with pytest.raises(TypeError) as exc_info:
            ExpensiveSingleton()
        assert "can't be instantiated directly." in str(exc_info.value)
