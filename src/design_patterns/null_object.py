"""Null Object design pattern."""

from abc import abstractmethod
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class IMedia(Protocol):
    """Interface of a media class."""

    @property
    @abstractmethod
    def title(self) -> str:
        """Media title"""

    @property
    @abstractmethod
    def creator(self) -> str:
        """Media creator"""


class Album(IMedia):
    """Concrete album media class."""

    def __init__(self, title: str, artist: str) -> None:
        self._title = title
        self._artist = artist

    @property
    def title(self) -> str:
        return self._title

    @property
    def creator(self) -> str:
        return self._artist


class Book(IMedia):
    """Concrete book media class."""

    def __init__(self, title: str, author: str) -> None:
        self._title = title
        self._author = author

    @property
    def title(self) -> str:
        return self._title

    @property
    def creator(self) -> str:
        return self._author


class NullMedia(IMedia):
    """Concrete no media class."""

    @property
    def title(self) -> str:
        return "N/A"

    @property
    def creator(self) -> str:
        return "N/A"


class MediaCollection:
    """Concrete client that uses IMedia classes."""

    def __init__(self, medias: Iterable[IMedia]) -> None:
        self._collection = {media.title: media for media in medias}

    def get_info(self, title: str) -> str:
        """Get info about a media item in the collection."""
        media = self._collection.get(title, NullMedia())
        return f"{media.title} by {media.creator}"
