"""Tests for the Abstract Factory design pattern."""

from typing import Any, Type

import pytest

from design_patterns.abstract_factory import (
    IStreamOperationFactory,
    ConcreteStreamDownloaderFactory,
    ConcreteStreamUploaderFactory,
)


class TestStreamOperationFactory:
    """Test suite for the Abstract Factory design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "stream_operation_factory, exp_read_res, exp_read_out, exp_write_out",
        (
            (
                ConcreteStreamDownloaderFactory,
                "bar",
                "Reading from default_url...\n",
                "Writing to default_file_path...\n",
            ),
            (
                ConcreteStreamUploaderFactory,
                "foo",
                "Reading from default_file_path...\n",
                "Writing to default_url...\n",
            ),
        ),
    )
    def test_concrete_factory(
        stream_operation_factory: Type[IStreamOperationFactory],
        exp_read_res: str,
        exp_read_out: str,
        exp_write_out: str,
        capsys: Any,
    ) -> None:
        """
        Test that the factory creates the correct combination of stream reader
        and writer.
        """
        downloader_factory = stream_operation_factory()
        stream_reader = downloader_factory.create_stream_reader()
        stream_writer = downloader_factory.create_stream_writer()

        read_res = stream_reader.read()
        assert read_res == exp_read_res
        out, _ = capsys.readouterr()
        assert out == exp_read_out

        write_res = stream_writer.write(read_res)
        assert write_res == len(read_res)
        out, _ = capsys.readouterr()
        assert out == exp_write_out
