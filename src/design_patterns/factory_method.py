"""Factory Method design pattern."""

from abc import abstractmethod
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol


class IStreamWriter(Protocol):
    """Interface of a stream writer class."""

    @abstractmethod
    def write(self, data: str) -> int:
        """Write `data` to a stream destination."""


class FileWriter(IStreamWriter):
    """Concrete stream writer class for writing to files."""

    def __init__(self, file_path: str):
        self.file_path = file_path

    def write(self, data: str) -> int:
        """Write `data` to a file destination."""
        print(f"Writing to {self.file_path}...")
        return len(data)


class NetworkWriter(IStreamWriter):
    """Concrete stream writer class for writing to a network connection."""

    def __init__(self, url: str):
        self.url = url

    def write(self, data: str) -> int:
        """Write `data` to a network destination."""
        print(f"Writing to {self.url}...")
        return len(data)


class IStreamWriterFactory(Protocol):
    """Factory interface for creating different types of stream writers"""

    @abstractmethod
    def create_stream_writer(self, stream_writer_type: str) -> IStreamWriter:
        """Create a stream writer."""


class ConcreteStreamWriterFactory(IStreamWriterFactory):
    """Concrete factory for creating different types of stream writers"""

    STREAM_WRITERS = {
        "file": (FileWriter, "default_file_path"),
        "network": (NetworkWriter, "default_url"),
    }

    def create_stream_writer(self, stream_writer_type: str) -> IStreamWriter:
        """Create a stream writer."""
        cls, default_dest = ConcreteStreamWriterFactory.STREAM_WRITERS[
            stream_writer_type
        ]
        stream_writer = cls(default_dest)
        return stream_writer
