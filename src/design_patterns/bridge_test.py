"""Tests for the Bridge design pattern."""

import sys
from typing import Any

import pytest

from design_patterns.bridge import (
    AlbumMedia,
    BookMedia,
    MovieMedia,
    ILayoutStrategy,
    ShortLayoutStrategy,
    LongLayoutStrategy,
)

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class TestLayoutMediaBridge:
    """Test suite for the Bridge design pattern."""

    album = AlbumMedia(
        "Slo Jamz",
        ("Michael", "Cyndi", "Tupac"),
        "Songs about the long process of making jam.",
        "oh_yeah.png",
    )
    book = BookMedia(
        "A Great Novel",
        ("J.R.R. Martin",),
        "It's great because it's really long.",
        "great_novel.png",
    )
    movie = MovieMedia(
        "Superhero Movie 7",
        ("Taika Russo",),
        "50% more superheroes than SM 6 - Director's Cut",
        "s7.png",
    )

    @staticmethod
    @pytest.mark.parametrize(
        "layout_strategy, exp_lines",
        (
            (ShortLayoutStrategy(book), ("A Great Novel by J.R.R. Martin",)),
            (
                LongLayoutStrategy(book),
                (
                    "A Great Novel by J.R.R. Martin",
                    "It's great because it's really long.",
                    "great_novel.png",
                ),
            ),
            (
                ShortLayoutStrategy(movie),
                ("Superhero Movie 7 directed by Taika Russo",),
            ),
            (
                LongLayoutStrategy(movie),
                (
                    "Superhero Movie 7 directed by Taika Russo",
                    "50% more superheroes than SM 6 - Director's Cut",
                    "s7.png",
                ),
            ),
            (ShortLayoutStrategy(album), ("Slo Jamz by Michael",)),
            (
                LongLayoutStrategy(album),
                (
                    "Slo Jamz by Michael, feat. Cyndi, feat. Tupac",
                    "Songs about the long process of making jam.",
                    "oh_yeah.png",
                ),
            ),
        ),
    )
    def test_bridge(
        layout_strategy: ILayoutStrategy,
        exp_lines: Iterable[str],
        capsys: Any,
    ) -> None:
        """
        Test that the layout strategy produces the correct output for a given media.
        """
        layout_strategy.print()
        out, _ = capsys.readouterr()
        assert out == "\n".join(exp_lines) + "\n"
