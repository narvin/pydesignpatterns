"""Tests for the Command design pattern."""

import sys
from typing import Optional, Tuple

import pytest

from design_patterns.command import CalculatorReceiver, CalculatorCommander


if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class TestCalculatorCommander:
    """Test suite for the Command design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "ops_and_args, exp_value",
        (
            ((("add", 5),), 5),
            ((("add", 5), ("undo", None)), 0),
            ((("add", 5), ("add", 5), ("undo", None)), 5),
            ((("add", 5), ("subtract", 2)), 3),
            ((("add", 5), ("subtract", 2), ("undo", None)), 5),
            ((("add", 5), ("subtract", 2), ("undo", None), ("undo", None)), 0),
            ((("add", 5), ("undo", None), ("undo", None)), 0),
        ),
    )
    def test_calculator(
        ops_and_args: Iterable[Tuple[str, Optional[float]]], exp_value: float
    ) -> None:
        """
        Test that the calculator commander can execute and undo commands on the
        calculator receiver.
        """
        calculator = CalculatorReceiver()
        operator = CalculatorCommander(calculator)
        for operation, arg in ops_and_args:
            if operation != "undo":
                getattr(operator, operation)(arg)
            else:
                operator.undo()
        assert calculator.value == exp_value
