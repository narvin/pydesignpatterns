"""Command design pattern."""

from abc import abstractmethod
from collections.abc import MutableSequence
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol


class CalculatorReceiver:
    """A concrete calculator class."""

    def __init__(self) -> None:
        self.value: float = 0

    def add(self, num: float) -> None:
        """Add a number to the current value."""
        self.value += num

    def subtract(self, num: float) -> None:
        """Subtract a number from the current value."""
        self.value -= num


class ICommand(Protocol):
    """Interface of a command class."""

    @abstractmethod
    def execute(self, num: float) -> None:
        """Execute the command."""

    @abstractmethod
    def undo(self) -> None:
        """Undo the command."""


class CalculatorAddCommand(ICommand):
    """A concrete command class to execute a calculator's add method."""

    def __init__(self, calculator: CalculatorReceiver) -> None:
        self.stack: MutableSequence[float] = []
        self.calculator = calculator

    def execute(self, num: float) -> None:
        self.calculator.add(num)
        self.stack.append(num)

    def undo(self) -> None:
        if self.stack:
            self.calculator.subtract(self.stack.pop())


class CalculatorSubtractCommand(ICommand):
    """A concrete command class to execute a calculator's subtract method."""

    def __init__(self, calculator: CalculatorReceiver) -> None:
        self.stack: MutableSequence[float] = []
        self.calculator = calculator

    def execute(self, num: float) -> None:
        self.calculator.subtract(num)
        self.stack.append(num)

    def undo(self) -> None:
        if self.stack:
            self.calculator.add(self.stack.pop())


class CalculatorCommander:
    """A concrete class to execute various commands on a calculator."""

    def __init__(self, calculator: CalculatorReceiver) -> None:
        self.stack: MutableSequence[ICommand] = []
        self.add_command = CalculatorAddCommand(calculator)
        self.subtract_command = CalculatorSubtractCommand(calculator)

    def add(self, num: float) -> None:
        """Execute the add command on the calculator."""
        self.add_command.execute(num)
        self.stack.append(self.add_command)

    def subtract(self, num: float) -> None:
        """Execute the add command on the calculator."""
        self.subtract_command.execute(num)
        self.stack.append(self.subtract_command)

    def undo(self) -> None:
        """Undo the last command."""
        if self.stack:
            self.stack.pop().undo()
