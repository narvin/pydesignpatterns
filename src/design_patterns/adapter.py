"""Adapter design pattern."""

from abc import abstractmethod
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class IStdoutWriter(Protocol):
    """Interface of a stdout writer class."""

    @abstractmethod
    def write(self, lines: Iterable[str]) -> None:
        """Print lines in to stdout."""


class StdoutWriter(IStdoutWriter):
    """Concrete stdout writer class."""

    def write(self, lines: Iterable[str]) -> None:
        for line in lines:
            print(line)


class IPrinter(Protocol):
    """Interface of a printer class."""

    @abstractmethod
    def print(self, lines: Iterable[str]) -> None:
        """Print lines in to stdout."""


class StdoutWriterToPrinterAdapter(IPrinter):
    """Concrete adapter class."""

    def __init__(self, stdoutwriter: IStdoutWriter) -> None:
        self.stdoutwriter = stdoutwriter

    def print(self, lines: Iterable[str]) -> None:
        self.stdoutwriter.write(lines)
