"""Abstract Factory design pattern."""

from abc import abstractmethod
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol


class IStreamReader(Protocol):
    """Interface of a stream reader class."""

    @abstractmethod
    def read(self) -> str:
        """Read from a stream source."""


class FileReader(IStreamReader):
    """Concrete stream reader class for reading from files."""

    def __init__(self, file_path: str):
        self.file_path = file_path

    def read(self) -> str:
        """Read from a file source."""
        print(f"Reading from {self.file_path}...")
        return "foo"


class NetworkReader(IStreamReader):
    """Concrete stream reader class for reading from files."""

    def __init__(self, url: str):
        self.url = url

    def read(self) -> str:
        """Read from a network source."""
        print(f"Reading from {self.url}...")
        return "bar"


class IStreamWriter(Protocol):
    """Interface of a stream writer class."""

    @abstractmethod
    def write(self, data: str) -> int:
        """Write `data` to a stream destination."""


class FileWriter(IStreamWriter):
    """Concrete stream writer class for writing to files."""

    def __init__(self, file_path: str):
        self.file_path = file_path

    def write(self, data: str) -> int:
        """Write `data` to a file destination."""
        print(f"Writing to {self.file_path}...")
        return len(data)


class NetworkWriter(IStreamWriter):
    """Concrete stream writer class for writing to a network connection."""

    def __init__(self, url: str):
        self.url = url

    def write(self, data: str) -> int:
        """Write `data` to a network destination."""
        print(f"Writing to {self.url}...")
        return len(data)


class IStreamOperationFactory(Protocol):
    """Factory interface for creating different types of stream writers"""

    @abstractmethod
    def create_stream_reader(self) -> IStreamReader:
        """Create a stream reader."""

    @abstractmethod
    def create_stream_writer(self) -> IStreamWriter:
        """Create a stream writer."""


class ConcreteStreamDownloaderFactory(IStreamOperationFactory):
    """Concrete factory for creating stream readers and writers to download files"""

    def create_stream_reader(self) -> IStreamReader:
        """Create a stream reader."""
        return NetworkReader("default_url")

    def create_stream_writer(self) -> IStreamWriter:
        """Create a stream writer."""
        return FileWriter("default_file_path")


class ConcreteStreamUploaderFactory(IStreamOperationFactory):
    """Concrete factory for creating stream readers and writers to upload files"""

    def create_stream_reader(self) -> IStreamReader:
        """Create a stream reader."""
        return FileReader("default_file_path")

    def create_stream_writer(self) -> IStreamWriter:
        """Create a stream writer."""
        return NetworkWriter("default_url")
