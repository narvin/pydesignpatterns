"""Iterator design pattern."""

from abc import ABCMeta, abstractmethod
from collections import deque
from typing import Optional, Type, TypeVar
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterator
else:
    from typing import Iterator


T_co = TypeVar("T_co", covariant=True)


class IIterator(Protocol[T_co]):
    """Interface of an iterator class."""

    @abstractmethod
    def has_next(self) -> bool:
        """Return `True` if the iterator can produce another item."""

    @abstractmethod
    def next(self) -> T_co:
        """Return the next item in the iterable."""


class IIterable(Protocol[T_co]):
    """Interface of an iterable class."""

    @abstractmethod
    def create_iterator(self) -> IIterator[T_co]:
        """Return an iterator to iterate over the iterable."""


class BinaryTreeNode:
    """A binary tree node with a left and right child."""

    def __init__(
        self,
        value: int = 0,
        left: Optional["BinaryTreeNode"] = None,
        right: Optional["BinaryTreeNode"] = None,
    ) -> None:
        self.value = value
        self.left = left
        self.right = right


class AbstractBinaryTreeIterator(IIterator[BinaryTreeNode], metaclass=ABCMeta):
    """Abstract class for a binary tree iterator."""

    @abstractmethod
    def __init__(self, root: Optional[BinaryTreeNode]) -> None:
        """Seed the iterator with the tree root."""


class BinaryTreeBFSIterator(AbstractBinaryTreeIterator):
    """Concrete class to iterate over a binary tree in BFS order."""

    def __init__(self, root: Optional[BinaryTreeNode]) -> None:
        self.queue = deque([root]) if root else deque([])

    def has_next(self) -> bool:
        return len(self.queue) > 0

    def next(self) -> BinaryTreeNode:
        if not deque:
            raise StopIteration
        curr = self.queue.popleft()
        if curr.left:
            self.queue.append(curr.left)
        if curr.right:
            self.queue.append(curr.right)
        return curr


class BinaryTreeDFSIterator(AbstractBinaryTreeIterator):
    """Concrete class to iterate over a binary tree in pre-order DFS order."""

    def __init__(self, root: Optional[BinaryTreeNode]) -> None:
        self.stack = [root] if root else []

    def has_next(self) -> bool:
        return len(self.stack) > 0

    def next(self) -> BinaryTreeNode:
        if not self.stack:
            raise StopIteration
        curr = self.stack.pop()
        if curr.right:
            self.stack.append(curr.right)
        if curr.left:
            self.stack.append(curr.left)
        return curr


def create_binary_tree_nodes_from_value_list(
    node_value_list: Iterator[Optional[int]],
) -> Optional[BinaryTreeNode]:
    """Create a binary tree from a list of node values."""
    # Create the nodes
    node_list = [
        BinaryTreeNode(value) if value is not None else None
        for value in node_value_list
    ]
    # Connect the nodes
    for idx, node in enumerate(node_list[: len(node_list) // 2]):
        if node:
            left_idx = 2 * idx + 1
            right_idx = 2 * idx + 2
            if left_idx < len(node_list):
                node.left = node_list[left_idx]
                if right_idx < len(node_list):
                    node.right = node_list[right_idx]
    # Return the root
    return node_list[0]


class BinaryTree(IIterable[BinaryTreeNode]):
    """
    A binary tree iterable that can iterate over its nodes using the specified
    iterator.
    """

    def __init__(
        self,
        binary_tree_iterator_class: Type[AbstractBinaryTreeIterator],
        node_value_list: Iterator[Optional[int]],
    ) -> None:
        self.root = create_binary_tree_nodes_from_value_list(node_value_list)
        self.binary_tree_iterator_class = binary_tree_iterator_class

    def create_iterator(self) -> IIterator[BinaryTreeNode]:
        return self.binary_tree_iterator_class(self.root)
