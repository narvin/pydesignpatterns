"""State design pattern."""

from abc import abstractmethod
from enum import Enum, auto
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol


class WheelEvent(Enum):
    """Option events."""

    WEEK_BEGINS = auto()
    EXPIRATION = auto()
    ASSIGNMENT = auto()


class IWheelState(Protocol):
    """Interface of a wheel option strategy state class."""

    @abstractmethod
    def transition_state(self, event: WheelEvent) -> "IWheelState":
        """Execute the correct transaction and return the next state."""


class WheelFlatState(IWheelState):
    """A concrete state class when the position is flat."""

    def transition_state(self, event: WheelEvent) -> IWheelState:
        if event == WheelEvent.WEEK_BEGINS:
            return WheelShortPutState()
        return self


class WheelShortPutState(IWheelState):
    """A concrete state class when the position is short a put."""

    def transition_state(self, event: WheelEvent) -> IWheelState:
        if event == WheelEvent.EXPIRATION:
            return WheelFlatState()
        if event == WheelEvent.ASSIGNMENT:
            return WheelLongUnderlyingState()
        return self


class WheelLongUnderlyingState(IWheelState):
    """A concrete state class when the position is long the underlying."""

    def transition_state(self, event: WheelEvent) -> IWheelState:
        if event == WheelEvent.WEEK_BEGINS:
            return WheelLongUnderlyingAndShortCallState()
        return self


class WheelLongUnderlyingAndShortCallState(IWheelState):
    """A concrete state class when the position is long the underlying."""

    def transition_state(self, event: WheelEvent) -> IWheelState:
        if event == WheelEvent.EXPIRATION:
            return WheelLongUnderlyingState()
        if event == WheelEvent.ASSIGNMENT:
            return WheelFlatState()
        return self


class Wheel:
    """A concrete wheel options strategy class."""

    def __init__(self) -> None:
        self.state: IWheelState = WheelFlatState()

    def process_event(self, event: WheelEvent) -> None:
        """Process a wheel event."""
        self.state = self.state.transition_state(event)
