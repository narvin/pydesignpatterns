"""Tests for the Adapter design pattern."""

from typing import Any
import sys

import pytest

from design_patterns.adapter import StdoutWriterToPrinterAdapter, StdoutWriter

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class TestStreamWriterFactory:
    """Test suite for the Adapter design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "lines, exp_out",
        (([], ""), (("foo", "bar", "baz"), "foo\nbar\nbaz\n")),
    )
    def test_adapter(lines: Iterable[str], exp_out: str, capsys: Any) -> None:
        """Test that the adapted StdoutWriter prints the correct output."""
        adapter = StdoutWriterToPrinterAdapter(StdoutWriter())
        adapter.print(lines)
        out, _ = capsys.readouterr()
        assert out == exp_out
