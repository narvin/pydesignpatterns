"""Tests for the Null Object design pattern."""

import pytest

from design_patterns.null_object import Album, Book, MediaCollection


class TestMediaCollection:
    """Test suite for the Null Object design pattern."""

    @staticmethod
    @pytest.fixture
    def media_collection() -> MediaCollection:
        """Dict of media title -> media"""
        return MediaCollection(
            (
                Album("Thriller", "Michael Jackson"),
                Book("The Hobbit", "J.R.R. Tolkien"),
                Book("Foundation", "Isaac Asimov"),
            )
        )

    @staticmethod
    @pytest.mark.parametrize(
        "title, exp_out",
        (
            ("Thriller", "Thriller by Michael Jackson"),
            ("Foundation", "Foundation by Isaac Asimov"),
            ("Bad", "N/A by N/A"),
        ),
    )
    def test_get(title: str, exp_out: str, media_collection: MediaCollection) -> None:
        """
        Test that the media collection returns a null record when a piece of media
        isn't found.
        """
        assert media_collection.get_info(title) == exp_out
