"""Tests for the Factory Method design pattern."""

from typing import Any

import pytest

from design_patterns.factory_method import ConcreteStreamWriterFactory


class TestStreamWriterFactory:
    """Test suite for the Factory Method design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "stream_writer_type, data, expected_output",
        (
            ("file", "hello", "Writing to default_file_path...\n"),
            ("network", "bye", "Writing to default_url...\n"),
        ),
    )
    def test_concrete_factory(
        stream_writer_type: str, data: str, expected_output: str, capsys: Any
    ) -> None:
        """Test that the factory creates the correct stream writer."""
        stream_writer_factory = ConcreteStreamWriterFactory()
        stream_writer = stream_writer_factory.create_stream_writer(stream_writer_type)
        count_written = stream_writer.write(data)
        assert count_written == len(data)
        out, _ = capsys.readouterr()
        assert out == expected_output
