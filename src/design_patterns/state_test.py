"""Tests for the State design pattern."""

import sys
from typing import Tuple, Type

import pytest

from design_patterns.state import (
    WheelEvent,
    IWheelState,
    WheelFlatState,
    WheelShortPutState,
    WheelLongUnderlyingState,
    WheelLongUnderlyingAndShortCallState,
    Wheel,
)


if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class TestWheel:
    """Test suite for the Command design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "events_and_states",
        (
            ((WheelEvent.WEEK_BEGINS, WheelShortPutState),),
            (
                (WheelEvent.WEEK_BEGINS, WheelShortPutState),
                (WheelEvent.EXPIRATION, WheelFlatState),
            ),
            (
                (WheelEvent.WEEK_BEGINS, WheelShortPutState),
                (WheelEvent.EXPIRATION, WheelFlatState),
                (WheelEvent.WEEK_BEGINS, WheelShortPutState),
            ),
            (
                (WheelEvent.WEEK_BEGINS, WheelShortPutState),
                (WheelEvent.ASSIGNMENT, WheelLongUnderlyingState),
                (WheelEvent.WEEK_BEGINS, WheelLongUnderlyingAndShortCallState),
                (WheelEvent.EXPIRATION, WheelLongUnderlyingState),
                (WheelEvent.WEEK_BEGINS, WheelLongUnderlyingAndShortCallState),
                (WheelEvent.ASSIGNMENT, WheelFlatState),
                (WheelEvent.WEEK_BEGINS, WheelShortPutState),
            ),
        ),
    )
    def test_wheel(
        events_and_states: Iterable[Tuple[WheelEvent, Type[IWheelState]]]
    ) -> None:
        """
        Test that the wheel transitions to the correct state based on the next event.
        """
        wheel = Wheel()
        for event, next_state_cls in events_and_states:
            wheel.process_event(event)
            assert isinstance(wheel.state, next_state_cls)
