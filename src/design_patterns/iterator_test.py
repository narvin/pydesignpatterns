"""Tests for the Iterator design pattern."""

import sys
from typing import Optional

import pytest

from design_patterns.iterator import (
    BinaryTree,
    BinaryTreeBFSIterator,
    BinaryTreeDFSIterator,
)

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterator
else:
    from typing import Iterator


class TestBinaryTree:
    """Test suite for the Adapter design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "node_value_list",
        ([1], [1, 2, 3], [1, 2, 3, 4, 5, 6], [1, 2, 3, 4, None, 5, 6]),
    )
    def test_iterate_bfs(node_value_list: Iterator[Optional[int]]) -> None:
        """Test that the binary tree iterates over itself in BFS order."""
        tree = BinaryTree(BinaryTreeBFSIterator, node_value_list)
        values = []
        iterator = tree.create_iterator()
        while iterator.has_next():
            values.append(iterator.next().value)
        assert values == [value for value in node_value_list if value is not None]

    @staticmethod
    @pytest.mark.parametrize(
        "node_value_list, exp_node_value_list",
        (
            ([1], [1]),
            ([1, 2, 3], [1, 2, 3]),
            ([1, 2, 3, 4, 5, 6], [1, 2, 4, 5, 3, 6]),
            ([1, 2, 3, 4, None, 5, 6], [1, 2, 4, 3, 5, 6]),
        ),
    )
    def test_iterate_dfs(
        node_value_list: Iterator[Optional[int]], exp_node_value_list: Iterator[int]
    ) -> None:
        """Test that the binary tree iterates over itself in a pre-order DFS order."""
        tree = BinaryTree(BinaryTreeDFSIterator, node_value_list)
        values = []
        iterator = tree.create_iterator()
        while iterator.has_next():
            values.append(iterator.next().value)
        assert values == list(exp_node_value_list)
