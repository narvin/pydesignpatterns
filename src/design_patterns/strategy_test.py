"""Tests for the Strategy design pattern."""

from typing import Any, Tuple

import pytest

from design_patterns.strategy import (
    ISaveStrategy,
    CSVSaveStrategy,
    DatabaseSaveStrategy,
    Client,
)


class TestStreamWriterFactory:
    """Test suite for the Strategy design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "save_strategy, record_info, exp_out",
        (
            (
                CSVSaveStrategy("default_file"),
                ("Narvin", "Singh", 29),
                "Saving record for Narvin Singh to CSV file 'default_file'...",
            ),
            (
                DatabaseSaveStrategy("default_db"),
                ("Marteena", "Singh", 22),
                "Saving record for Marteena Singh to database 'default_db'...",
            ),
        ),
    )
    def test_strategy(
        save_strategy: ISaveStrategy,
        record_info: Tuple[str, str, int],
        exp_out: str,
        capsys: Any,
    ) -> None:
        """Test that the client uses the strategy to save records."""
        client = Client(save_strategy)
        record = client.create_record(*record_info)
        client.save_record(record)
        out, _ = capsys.readouterr()
        assert out == f"{exp_out}\n"
