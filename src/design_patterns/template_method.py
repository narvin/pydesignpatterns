"""Template Method design pattern."""

from abc import ABCMeta, abstractmethod
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import final
else:
    from typing_extensions import final


class AnalyzerTemplate(metaclass=ABCMeta):
    """Abstract template for an analyzer class."""

    def __init__(self, file: str) -> None:
        self._file = file

    @abstractmethod
    def do_analysis_step(self) -> None:
        """Abstract method to perform an analysis step."""

    @final
    def analyze(self) -> None:
        """Concrete method composed of abstract methods to perform analysis."""
        print("Doing pre-analysis...")
        self.do_analysis_step()
        print("Doing post-analysis...")


class CSVAnalyzer(AnalyzerTemplate):
    """Concrete anaylzer class to analyse CSV data."""

    def do_analysis_step(self) -> None:
        print(f"Analyzing CSV data for file '{self._file}'...")


class JSONAnalyzer(AnalyzerTemplate):
    """Concrete anaylzer class to analyse JSON data."""

    def do_analysis_step(self) -> None:
        print(f"Analyzing JSON data for file '{self._file}'...")


class XMLAnalyzer(AnalyzerTemplate):
    """Concrete anaylzer class to analyse XML data."""

    def do_analysis_step(self) -> None:
        print(f"Analyzing XML data for file '{self._file}'...")
