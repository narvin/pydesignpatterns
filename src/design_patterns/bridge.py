"""Bridge design pattern."""

from abc import abstractmethod
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Sequence
else:
    from typing import Sequence


class IMedia(Protocol):
    """Interface of a media class."""

    title: str
    creators: Sequence[str]
    description: str
    image: str

    @property
    @abstractmethod
    def first_creator_prep(self) -> str:
        """The preposition or phrase to use preceding the first creator."""

    @property
    @abstractmethod
    def additional_creators_prep(self) -> str:
        """The preposition or phrase to use preceding additonal creators."""


class AlbumMedia(IMedia):
    """Concrete media class of an album."""

    def __init__(
        self, title: str, artists: Sequence[str], description: str, image: str
    ) -> None:
        self.title = title
        self.creators = artists
        self.description = description
        self.image = image

    @property
    def first_creator_prep(self) -> str:
        return " by " if self.creators else ""

    @property
    def additional_creators_prep(self) -> str:
        return ", feat. " if self.creators else ""


class BookMedia(IMedia):
    """Concrete media class of a book."""

    def __init__(
        self, title: str, authors: Sequence[str], description: str, image: str
    ) -> None:
        self.title = title
        self.creators = authors
        self.description = description
        self.image = image

    @property
    def first_creator_prep(self) -> str:
        return " by " if self.creators else ""

    @property
    def additional_creators_prep(self) -> str:
        return " and " if self.creators else ""


class MovieMedia(IMedia):
    """Concrete media class of a movie."""

    def __init__(
        self, title: str, directors: Sequence[str], description: str, image: str
    ) -> None:
        self.title = title
        self.creators = directors
        self.description = description
        self.image = image

    @property
    def first_creator_prep(self) -> str:
        return " directed by " if self.creators else ""

    @property
    def additional_creators_prep(self) -> str:
        return " and " if self.creators else ""


class ILayoutStrategy(Protocol):
    """Interface of a layout strategy class."""

    media: IMedia

    @abstractmethod
    def print(self) -> None:
        """Print a layout for a piece of media."""


class ShortLayoutStrategy(ILayoutStrategy):
    """Concrete layout strategy class for a short layout."""

    def __init__(self, media: IMedia) -> None:
        self.media = media

    def print(self) -> None:
        creator = (
            f"{self.media.first_creator_prep}{self.media.creators[0]}"
            if self.media.creators
            else ""
        )
        print(f"{self.media.title}{creator}")


class LongLayoutStrategy(ILayoutStrategy):
    """Concrete layout strategy class for a long layout."""

    def __init__(self, media: IMedia) -> None:
        self.media = media

    def print(self) -> None:
        if self.media.creators:
            creators = (
                f"{self.media.first_creator_prep}"
                + self.media.additional_creators_prep.join(self.media.creators)
            )
        else:
            creators = ""
        print(f"{self.media.title}{creators}")
        print(self.media.description)
        print(self.media.image)
