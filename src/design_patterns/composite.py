"""Composite design pattern."""

from abc import abstractmethod
import sys
from typing import Optional

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class IFSItem(Protocol):
    """Interface of a file system item class."""

    @property
    @abstractmethod
    def name(self) -> str:
        """The item name."""

    @abstractmethod
    def list_contents(self) -> Iterable[str]:
        """List all the contents of the file system item."""


class Folder(IFSItem):
    """Concrete composite file system item class."""

    def __init__(self, name: str, contents: Optional[Iterable[IFSItem]] = None) -> None:
        self._name = name
        self._contents = contents if contents else []

    @property
    def name(self) -> str:
        return self._name

    def list_contents(self) -> Iterable[str]:
        return (
            self.name,
            *[
                f"{self.name}/{name}"
                for fsitem in self._contents
                for name in fsitem.list_contents()
            ],
        )


class File(IFSItem):
    """Concrete leaf file system item class."""

    def __init__(self, name: str) -> None:
        self._name = name

    @property
    def name(self) -> str:
        return self._name

    def list_contents(self) -> Iterable[str]:
        return (self.name,)
