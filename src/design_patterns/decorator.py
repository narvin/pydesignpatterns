"""Decorator design pattern."""

from abc import ABCMeta, abstractmethod
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class IPrinter(Protocol):
    """Interface of a printer class."""

    @abstractmethod
    def print(self, lines: Iterable[str]) -> None:
        """Print lines in some manner to stdout."""


class Printer(IPrinter):
    """Concrete printer class."""

    def print(self, lines: Iterable[str]) -> None:
        """Print each line to stdout."""
        for line in lines:
            print(line)


class AbstractPrinterDecorator(IPrinter, metaclass=ABCMeta):
    """Interface of a printer decorator class."""

    def __init__(self, printer: IPrinter) -> None:
        self.printer = printer


class LineNumberPrinterDecorator(AbstractPrinterDecorator):
    """Concrete printer decorator class to add line numbers."""

    def print(self, lines: Iterable[str]) -> None:
        """Print each line to stdout."""
        self.printer.print((f"{idx + 1}\t{line}" for idx, line in enumerate(lines)))


class UpperCasePrinterDecorator(AbstractPrinterDecorator):
    """Concrete printer decorator class to upper-case line."""

    def print(self, lines: Iterable[str]) -> None:
        """Print each line to stdout."""
        self.printer.print((line.upper() for line in lines))


class ReversePrinterDecorator(AbstractPrinterDecorator):
    """Concrete printer decorator class to reverse lines."""

    def print(self, lines: Iterable[str]) -> None:
        """Print each line to stdout."""
        self.printer.print((line[::-1] for line in lines))
