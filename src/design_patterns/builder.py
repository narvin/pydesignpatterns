"""Builder design pattern."""

from abc import abstractmethod
import sys
from typing import Optional

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class Desktop:
    """Concrete desktop class that is inconvenient to create using `__init__`."""

    def __init__(
        self,
        operating_system: Optional[str] = None,
        text_editor: Optional[str] = None,
        version_control_system: Optional[str] = None,
        programming_languages: Optional[Iterable[str]] = None,
    ) -> None:
        self.operating_system = operating_system
        self.text_editor = text_editor
        self.version_control_system = version_control_system
        self.programming_languages = programming_languages


class IDesktopBuilder(Protocol):
    """Interface of desktop builder class."""

    @abstractmethod
    def install_operating_system(self) -> None:
        """Install the operating system."""

    @abstractmethod
    def install_text_editor(self) -> None:
        """Install the text editor."""

    @abstractmethod
    def install_version_control_system(self) -> None:
        """Install the version control system."""

    @abstractmethod
    def install_programming_languages(self) -> None:
        """Install the programming languages."""

    @abstractmethod
    def get_desktop(self) -> Desktop:
        """Get the desktop."""


class DevDesktopBuilder(IDesktopBuilder):
    """
    Concrete developer desktop builder class were the programming languages can
    be specified.
    """

    def __init__(self, programming_languages: Optional[Iterable[str]]) -> None:
        self.desktop = Desktop()
        self.programming_languages = programming_languages

    def install_operating_system(self) -> None:
        self.desktop.operating_system = "linux"

    def install_text_editor(self) -> None:
        self.desktop.text_editor = "vim"

    def install_version_control_system(self) -> None:
        self.desktop.version_control_system = "git"

    def install_programming_languages(self) -> None:
        self.desktop.programming_languages = self.programming_languages

    def get_desktop(self) -> Desktop:
        return self.desktop


class UserDesktopBuilder(IDesktopBuilder):
    """Concrete user desktop builder class where no parameters need to be specified."""

    def __init__(self) -> None:
        self.desktop = Desktop()

    def install_operating_system(self) -> None:
        self.desktop.operating_system = "windows"

    def install_text_editor(self) -> None:
        self.desktop.text_editor = "notepad"

    def install_version_control_system(self) -> None:
        self.desktop.version_control_system = None

    def install_programming_languages(self) -> None:
        self.desktop.programming_languages = None

    def get_desktop(self) -> Desktop:
        return self.desktop


class DesktopCreator:
    """Concrete class to build a desktop using a desktop builder."""

    def __init__(self, desktop_builder: IDesktopBuilder) -> None:
        self.desktop_builder = desktop_builder

    def build_desktop(self) -> None:
        """Build the desktop."""
        self.desktop_builder.install_operating_system()
        self.desktop_builder.install_text_editor()
        self.desktop_builder.install_version_control_system()
        self.desktop_builder.install_programming_languages()

    def get_desktop(self) -> Desktop:
        """Return the desktop."""
        return self.desktop_builder.get_desktop()
