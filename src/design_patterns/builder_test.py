"""Tests for the Builder design pattern."""

import pytest

from design_patterns.builder import (
    Desktop,
    IDesktopBuilder,
    DevDesktopBuilder,
    UserDesktopBuilder,
    DesktopCreator,
)


class TestDesktopBuilder:
    """Test suite for the Builder design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "builder, exp_desktop",
        (
            (
                DevDesktopBuilder(("python", "javascript")),
                Desktop("linux", "vim", "git", ("python", "javascript")),
            ),
            (UserDesktopBuilder(), Desktop("windows", "notepad", None, None)),
        ),
    )
    def test_build(builder: IDesktopBuilder, exp_desktop: Desktop) -> None:
        """Test that the desktop builder builds a correctly configured desktop."""
        desktop_creator = DesktopCreator(builder)
        desktop_creator.build_desktop()
        act_desktop = desktop_creator.get_desktop()
        assert (
            getattr(act_desktop, attr) == getattr(exp_desktop, attr)
            for attr in (
                "operating_system",
                "text_editor",
                "version_control_system",
                "programming_languages",
            )
        )
