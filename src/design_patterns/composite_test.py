"""Tests for the Composite design pattern."""

import sys

import pytest

from design_patterns.composite import IFSItem, Folder, File

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class TestFSItem:
    """Test suite for the Composite design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "fsitem, exp_out",
        (
            (File("file.txt"), ("file.txt",)),
            (
                Folder("my_dir", (File("file.txt"),)),
                (
                    "my_dir",
                    "my_dir/file.txt",
                ),
            ),
            (
                Folder("my_dir", (Folder("my_subdir"), File("file.txt"))),
                (
                    "my_dir",
                    "my_dir/my_subdir",
                    "my_dir/file.txt",
                ),
            ),
            (
                Folder(
                    "my_dir",
                    (
                        Folder("my_subdir", (File("foo.txt"), File("bar.txt"))),
                        File("file.txt"),
                    ),
                ),
                (
                    "my_dir",
                    "my_dir/my_subdir",
                    "my_dir/my_subdir/foo.txt",
                    "my_dir/my_subdir/bar.txt",
                    "my_dir/file.txt",
                ),
            ),
        ),
    )
    def test_list_contents(fsitem: IFSItem, exp_out: Iterable[str]) -> None:
        """Test that the composite FSItem lists itself and all its contents."""
        assert fsitem.list_contents() == exp_out
