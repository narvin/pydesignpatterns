"""Tests for the Proxy design pattern."""

from typing import Any

import pytest

from design_patterns.proxy import (
    WebResourceRemoteProxy,
    DatabaseQueryVirtualProxy,
    FileProtectionProxy,
)


class TestRemoteProxy:
    """Test suite for the Remote Proxy design pattern."""

    @staticmethod
    def test_remote_proxy(capsys: Any) -> None:
        """Test that the proxy calls out to the remote object."""
        remote_proxy = WebResourceRemoteProxy("foobar")
        out, _ = capsys.readouterr()
        assert out == ""
        assert remote_proxy.value == "foobar is cool"
        out, _ = capsys.readouterr()
        assert out == "\n".join(
            (
                "Connecting to https://example.com/foobar...",
                "Downloading from https://example.com/foobar...",
                "",
            )
        )


class TestVirtualProxy:
    """Test suite for the Virtual Proxy design pattern."""

    @staticmethod
    def test_virtual_proxy(capsys: Any) -> None:
        """
        Test that the proxy doesn't create the expensive query until information
        about the query is actually requested.
        """
        virtual_proxy = DatabaseQueryVirtualProxy("expensive query")
        out, _ = capsys.readouterr()
        assert out == ""
        assert virtual_proxy.num_records == 1000
        out, _ = capsys.readouterr()
        assert out == "Executing query 'expensive query'...\n"


class TestProtectionProxy:
    """Test suite for the Protection Proxy design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "user, exp_res",
        (("good_user", "foobar"), ("bad_user", "")),
    )
    def test_protection_proxy(user: str, exp_res: str) -> None:
        """Test that the proxy only allows authorized requests to the file."""
        protection_proxy = FileProtectionProxy(user, "default_file")
        assert protection_proxy.read() == exp_res
