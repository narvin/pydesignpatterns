"""Strategy design pattern."""

from abc import abstractmethod
import sys

if sys.version_info[:2] >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol


class Record:
    """A record class."""

    def __init__(self, first: str, last: str, age: int) -> None:
        self.first = first
        self.last = last
        self.age = age


class ISaveStrategy(Protocol):
    """Interface of a save strategy class."""

    @abstractmethod
    def save(self, record: Record) -> None:
        """Save a record."""


class CSVSaveStrategy(ISaveStrategy):
    """Concrete save strategy class to save a record to a CSV file."""

    def __init__(self, file_path: str) -> None:
        self.file_path = file_path

    def save(self, record: Record) -> None:
        print(
            f"Saving record for {record.first} {record.last} to "
            + f"CSV file '{self.file_path}'..."
        )


class DatabaseSaveStrategy(ISaveStrategy):
    """Concrete save strategy class to save a record to a database."""

    def __init__(self, db_name: str) -> None:
        self.db_name = db_name

    def save(self, record: Record) -> None:
        print(
            f"Saving record for {record.first} {record.last} to "
            + f"database '{self.db_name}'..."
        )


class Client:
    """Client class that uses a strategy to save records."""

    def __init__(self, save_strategy: ISaveStrategy) -> None:
        self.save_strategy = save_strategy

    def create_record(self, first: str, last: str, age: int) -> Record:
        """Create and return a record."""
        return Record(first, last, age)

    def save_record(self, record: Record) -> None:
        """Save a record using the save strategy."""
        self.save_strategy.save(record)
