"""Tests for the Template Method design pattern."""

from typing import Any

import pytest

from design_patterns.template_method import (
    AnalyzerTemplate,
    CSVAnalyzer,
    JSONAnalyzer,
    XMLAnalyzer,
)


class TestAnalyzeTemplate:
    """Test suite for the Template Method design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "analyzer, exp_out",
        (
            (CSVAnalyzer("data.csv"), "Analyzing CSV data for file 'data.csv'..."),
            (JSONAnalyzer("data.json"), "Analyzing JSON data for file 'data.json'..."),
            (XMLAnalyzer("data.xml"), "Analyzing XML data for file 'data.xml'..."),
        ),
    )
    def test_analyze(analyzer: AnalyzerTemplate, exp_out: str, capsys: Any) -> None:
        """Test that the analyze method produces the correct output."""
        analyzer.analyze()
        out, _ = capsys.readouterr()
        assert out == f"Doing pre-analysis...\n{exp_out}\nDoing post-analysis...\n"
