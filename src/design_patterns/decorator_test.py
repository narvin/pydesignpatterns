"""Tests for the Decorator design pattern."""

from functools import reduce
from typing import Any, Type
import sys

import pytest

from design_patterns.decorator import (
    IPrinter,
    AbstractPrinterDecorator,
    LineNumberPrinterDecorator,
    UpperCasePrinterDecorator,
    ReversePrinterDecorator,
    Printer,
)

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable, Sequence
else:
    from typing import Iterable, Sequence


class TestStreamWriterFactory:
    """Test suite for the Decorator design pattern."""

    @staticmethod
    @pytest.mark.parametrize(
        "printer_decorators, lines, exp_lines",
        (
            (
                (LineNumberPrinterDecorator,),
                ("foo", "bar", "baz"),
                ("1\tfoo", "2\tbar", "3\tbaz"),
            ),
            (
                (UpperCasePrinterDecorator,),
                ("foo", "bar", "baz"),
                ("FOO", "BAR", "BAZ"),
            ),
            (
                (ReversePrinterDecorator,),
                ("foo", "bar", "baz"),
                ("oof", "rab", "zab"),
            ),
            (
                (
                    UpperCasePrinterDecorator,
                    ReversePrinterDecorator,
                    LineNumberPrinterDecorator,
                ),
                ("foo", "bar", "baz"),
                ("1\tOOF", "2\tRAB", "3\tZAB"),
            ),
        ),
    )
    def test_decorator(
        printer_decorators: Sequence[Type[AbstractPrinterDecorator]],
        lines: Iterable[str],
        exp_lines: Iterable[str],
        capsys: Any,
    ) -> None:
        """Test that the decorated printer prints the correct output."""
        printer: IPrinter = Printer()
        printer = reduce(
            lambda curr_printer, next_decorator: next_decorator(curr_printer),
            reversed(printer_decorators),
            printer,
        )
        printer.print(lines)
        out, _ = capsys.readouterr()
        assert out == "\n".join(exp_lines) + "\n"
