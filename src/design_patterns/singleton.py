"""Singleton design pattern."""

import sys
from typing import Any, Optional

if sys.version_info[:2] >= (3, 8):
    from typing import final
else:
    from typing_extensions import final


class Uncallable(type):
    """
    Metaclass to prevent instance classes from being called so they can't be
    instantiated directly.
    """

    def __call__(cls, *args: object) -> None:
        raise TypeError(
            f"{cls.__module__}.{cls.__qualname__} can't be instantiated directly."
        )

    def _create(cls: "Uncallable", *args: object) -> Any:
        """
        Instance classes can be instantiated using this method which invokes the
        `__call__` method on the base class of `Uncallable`, i.e., `type.__call__`.

        TODO: Figure out how to annotate the return type as `cls`, e.g.,

            def _create(cls: Uncallable, ...) -> cls:
        """
        return super().__call__(*args)


@final
class DatabaseSingleton(metaclass=Uncallable):
    """Database singleton class."""

    _instance: Optional["DatabaseSingleton"] = None

    def __init__(self, conn_str: str) -> None:
        self.conn_str = conn_str

    @classmethod
    def get_instance(cls, conn_str: str) -> "DatabaseSingleton":
        """Create the one instance of `cls` if it doesn't exist, and return it."""
        if cls._instance is None:
            # Since `cls` is an instance of its metaclassm the `_create` method
            # is looked up on the metaclass, and `cls` is implicitly passed as the
            # first parameter
            cls._instance = cls._create(conn_str)
        return cls._instance


@final
class ExpensiveSingleton(metaclass=Uncallable):
    """Singleton class that is expensive to create."""

    _instance: Optional["ExpensiveSingleton"] = None

    def __init__(self) -> None:
        self.value = "Some expensive computation"

    @classmethod
    def get_instance(cls) -> "ExpensiveSingleton":
        """Create the one instance of `cls` if it doesn't exist, and return it."""
        if cls._instance is None:
            cls._instance = cls._create()
        return cls._instance
